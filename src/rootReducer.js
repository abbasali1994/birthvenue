import { combineReducers } from 'redux';
import user from './reducer/user';

const createRootReducer = () => combineReducers({user});

export default createRootReducer;