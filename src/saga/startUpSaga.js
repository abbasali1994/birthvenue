import { put, take, fork, call, takeLatest } from 'redux-saga/effects';
import { initializeWeb3, isMetamaskInstalled, askPermission, getNetworkId, metaMaskAccountsChanged,  getAccountAddress, getNetworkName, sendEth, userEthBalance } from '../utils/index';
import { updateAccount, updateNetwork, updateNetworkID, updateIsLoading, updateBalance } from '../reducer/user';
import { eventChannel } from 'redux-saga';
function* startSaga() {
  yield takeLatest("CONNECT_WALLET", connectWallet);
  yield takeLatest("TRANSFER_TOKEN", transferToken);
  yield fork(watchMetaMaskAccountChange);
}

function* connectWallet() {
  try {
    yield put(updateIsLoading(true))
    const isMetamask = yield isMetamaskInstalled();
    if (isMetamask) {
      initializeWeb3()
      yield askPermission()
      const networkId = yield getNetworkId()
      const network = yield getNetworkName()
      const account = yield getAccountAddress()
      const balance = yield userEthBalance(account)
      yield put(updateAccount(account))
      yield put(updateNetworkID(networkId))
      yield put(updateNetwork(network))
      yield put(updateBalance(balance))
    }
  } catch (error) {
    console.log(error)
  }
  yield put(updateIsLoading(false))
}

function createMetaMaskAccountChannel() {
  return eventChannel(emit => {
    metaMaskAccountsChanged(account => {
      emit(account)
    });
    return () => {
      console.log('Account changed');
    }
  })
}

export function* watchMetaMaskAccountChange() {
  const accountChannel = yield call(createMetaMaskAccountChannel)
  while (true) {
    try {
      yield take(accountChannel)
      const networkId = yield getNetworkId()
      const network = yield getNetworkName()
      const account = yield getAccountAddress()
      const balance = yield userEthBalance(account)
      yield put(updateAccount(account))
      yield put(updateNetworkID(networkId))
      yield put(updateNetwork(network))
      yield put(updateBalance(balance))
    } catch (err) {
      console.error('error in Channel:', err)
    }
  }
}

function* transferToken(action) {
  
  const {reciever, sender, amount} = action.payload
  try {
    yield put(updateIsLoading(true))
    yield sendEth(reciever, sender, amount)
    const balance = yield userEthBalance(sender)
    yield put(updateBalance(balance))
  } catch (error) {
    console.log(error)
  }
  yield put(updateIsLoading(false))
};


export default startSaga;
