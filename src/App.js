import "./App.css";
import { startSaga } from "./rootSaga";
import { connect } from "react-redux";
import { createAction } from "@reduxjs/toolkit";
import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import { Backdrop } from '@material-ui/core';
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import LockOpenOutlinedIcon from "@material-ui/icons/LockOpenOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

const connectWallet = createAction("CONNECT_WALLET");
const transferToken = createAction("TRANSFER_TOKEN");
const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatarClose: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  avatarOpen: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const App = (props) => {
  const classes = useStyles();
  const [amount, setAmount] = useState(0);
  const [reciever, setReciever] = useState("");

  return (
    <>
    <Backdrop className={classes.backdrop}
        open={props.isLoading}>
        <Grid style={{ textAlign: "center" }}>
          <Typography className={classes.typography} style={{ textAlign: "center" }}>Transaction in progress</Typography>
        </Grid>
      </Backdrop>
    
    <Container component="main" maxWidth="md">
    
      <CssBaseline />
      
      <div className={classes.paper}>
      <Typography component="h5" variant="h5">
              Please install Metamask extension if wallet not setup Already
            </Typography>
        {props.address ? (
          <>
            <Avatar className={classes.avatarOpen}>
              <LockOpenOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              {props.address}
            </Typography>
          </>
        ) : (
          <>
            <Avatar className={classes.avatarClose}>
              <Button onClick={() => props.connectWallet()}>
                <LockOutlinedIcon />
              </Button>
            </Avatar>
            <Typography component="h1" variant="h5">
              Click Icon to Connect Metamask Wallet
            </Typography>
          </>
        )}
        <Grid>
            <Grid item>
            {props.network? "Network: "+props.network:""}
            </Grid>
            <Grid item>
            {props.balance? "Balance: "+props.balance:""}
            </Grid>
          </Grid>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="reciever"
            label="Reciever Address"
            name="reciever"
            autoComplete="reciever"
            autoFocus
            onChange={(event) => setReciever(event.target.value)}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="amount"
            label="ETH Amount"
            type="number"
            id="amount"
            onChange={(event) => setAmount(event.target.value)}
          />
          <Button
            onClick={(event)=>{
              event.preventDefault()
              props.transferToken({reciever, sender:props.address, amount})}}
            type="button"
            fullWidth
            variant="contained"
            color="primary"
            disabled={!props.address||!reciever||!amount}
          >
            Send
          </Button>
        </form>

      </div>
    </Container>
    </>
  );
};

const mapStateToProps = (state) => ({
  address: state.user.address,
  network: state.user.network,
  balance: state.user.balance,
  isLoading: state.user.isLoading,
});

const WrappedComponent = connect(mapStateToProps, { connectWallet,transferToken })(App);

const index = () => {
  startSaga();
  return <WrappedComponent />;
};

export default index;
