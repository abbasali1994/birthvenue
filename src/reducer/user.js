import { createSlice } from '@reduxjs/toolkit'

const configSlice = createSlice({
  name: 'user',
  initialState: {
    isLoading: false,
  },
  reducers: {
    startUpdateAccount() { },
    updateAccount(state, action) {
      state.address = action.payload;
    },
    updateNetwork(state, action) {
      state.network = action.payload
    },
    updateNetworkID(state, action) {
      state.networkID = action.payload
    },
    updateIsLoading(state, action) {
      state.isLoading = action.payload
    },
    updateBalance(state, action) {
      state.balance = action.payload
    },
  }
})

const { actions, reducer } = configSlice;

export const { updateAccount, updateNetwork, updateNetworkID, updateIsLoading, updateBalance } = actions;

export default reducer;
