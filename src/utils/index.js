import '@metamask/legacy-web3';
import Web3 from 'web3';

export const isMetamaskInstalled = () => {
  return !!window.ethereum || !!window.web3;
}

export const initializeWeb3 = async () => {
  if (window.ethereum) {
    window.web3 = new Web3(window.ethereum);
    // window.ethereum.autoRefreshOnNetworkChange = false;
  } else {
    console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
  }
}

export const askPermission = async () => {
  try {
    await window.ethereum.request({ method: 'eth_requestAccounts' });
  } catch (error) {
    throw new Error(error);
  }
}

export const getNetworkId = async () => window.web3.eth.net.getId();

export const getAccounts = async () => window.web3.eth.getAccounts();

export const getAccountAddress = async () => {
  const [address] = await window.web3.eth.getAccounts();
  return address
}

export const getNetworkName = () => {
  return window.web3.eth.net.getNetworkType()
}

export const isUserLoggedIn = () => new Promise((resolve, reject) => {
  window.web3.eth.getAccounts((err, accounts) => {
    if (err != null) {
      console.log(err)
      reject(err);
    }
    resolve(accounts.length !== 0)
  });
});

export const sendEth = async (receiver,sender,amount) => {
  var weiValue = window.web3.utils.toWei(amount, 'ether');
  await window.web3.eth.sendTransaction({to:receiver, from:sender, value:weiValue})
}

export const userEthBalance = async (address) => {
  let balance = await window.web3.eth.getBalance(address);
  return parseInt(balance) / 10**18
}

export const metaMaskAccountsChanged = (callback) => {
  window.ethereum.on('chainChanged', callback)
  window.ethereum.on('accountsChanged', callback);
}
